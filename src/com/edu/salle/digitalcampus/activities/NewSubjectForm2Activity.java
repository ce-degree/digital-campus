package com.edu.salle.digitalcampus.activities;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.adapter.SubjectStudentAdapter;
import com.edu.salle.digitalcampus.dao.StudentDAO;
import com.edu.salle.digitalcampus.objects.Student;
import com.edu.salle.digitalcampus.util.Tuple2;

public class NewSubjectForm2Activity extends BaseActivity {

	private Tuple2<ArrayList<Student>, ArrayList<Boolean>> data;
	private ListView myListView;
	private Button btnBack;
	private Button btnNext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_subject_form2);
		// Show the Up button in the action bar.
		setupActionBar();

		associateControls();
		prepareActivity();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_subject_form2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void associateControls() {
		myListView = (ListView) findViewById(R.id.subject_student_listview);
		btnBack = (Button) findViewById(R.id.subject_student_back);
		btnNext = (Button) findViewById(R.id.subject_student_next);
	}

	private void prepareActivity() {
		if (getIntent().getExtras() != null) {
			if (getIntent().getExtras().getBoolean(
					BaseActivity.PARAM_FROM_NEXT_STEP_ASSISTANT, false) == true) {
				Toast.makeText(
						NewSubjectForm2Activity.this.getApplicationContext(),
						"Entrando en getExtras", Toast.LENGTH_LONG).show();
				data = getApp().getAssociatedStudents();
			}
		} else {
			data = loadSubjects();
		}

		getApp().setAssociatedStudents(data);
		
		SubjectStudentAdapter adapter = new SubjectStudentAdapter(
				getApplicationContext(), R.layout.subject_student_list_item,
				getApp().getAssociatedStudents());
		myListView.setAdapter(adapter);

		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Toast.makeText(
						NewSubjectForm2Activity.this.getApplicationContext(),
						"Back Btn", Toast.LENGTH_LONG).show();

			}
		});

		btnNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Toast.makeText(
						NewSubjectForm2Activity.this.getApplicationContext(),
						"Next Btn", Toast.LENGTH_LONG).show();
				SubjectStudentAdapter adapter = (SubjectStudentAdapter) myListView
						.getAdapter();

				getApp().setAssociatedStudents(adapter.getData());

				Intent i = new Intent(getApplicationContext(),
						NewSubjectForm3Activity.class);
				startActivity(i);
				finish();
			}

		});

		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						NewSubjectForm1Activity.class);
				i.putExtra(BaseActivity.PARAM_FROM_NEXT_STEP_ASSISTANT, true);
				startActivity(i);
				finish();

			}
		});
	}

	private Tuple2<ArrayList<Student>, ArrayList<Boolean>> loadSubjects() {
		Tuple2<ArrayList<Student>, ArrayList<Boolean>> data = new Tuple2<ArrayList<Student>, ArrayList<Boolean>>(
				new ArrayList<Student>(), new ArrayList<Boolean>());
		
		StudentDAO studentDAO = new StudentDAO(getApplicationContext());

		if (!studentDAO.isAnyTable(true))
			Log.d("DATABASE", "No hay tablas!!");
		if (studentDAO.isTableExists("Student", true))
			data.set_1((ArrayList<Student>)studentDAO.getAllStudents());
		for(Student student :data.get_1()) {
			data.get_2().add(false);
		}

		return data;
	}
}
