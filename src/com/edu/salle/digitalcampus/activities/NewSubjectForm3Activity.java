package com.edu.salle.digitalcampus.activities;

import java.util.ArrayList;
import java.util.List;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.R.layout;
import com.edu.salle.digitalcampus.R.menu;
import com.edu.salle.digitalcampus.adapter.ChapterAdapter;
import com.edu.salle.digitalcampus.adapter.SubjectStudentAdapter;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

public class NewSubjectForm3Activity extends BaseActivity {

	private Button btnBack;
	private Button btnNext;
	private Button btnInsert;
	private EditText editTChapter;
	private ListView myListView;
	private ChapterAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_subject_form3);

		List<String> chapters = new ArrayList<String>();
		chapters.add("Hola");
		chapters.add("Adeu");

		getApp().getNewSubject().setChapters(chapters);

		associateControls();
		prepareActivity();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_subject_form3, menu);
		return true;
	}

	private void associateControls() {
		this.myListView = (ListView) findViewById(R.id.chapters_listview);
		this.btnInsert = (Button) findViewById(R.id.new_subject_form3_insert_new_chapter);
		this.btnBack = (Button) findViewById(R.id.subject_student_back);
		this.btnNext = (Button) findViewById(R.id.subject_student_next);
		this.editTChapter = (EditText) findViewById(R.id.new_subject_form3_new_chapter);
	}

	private void prepareActivity() {

		this.adapter = new ChapterAdapter(getApplicationContext(),
				R.layout.chapter_list_item, getApp().getNewSubject()
						.getChapters());

		this.myListView.setAdapter(adapter);

		this.btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						NewSubjectForm2Activity.class);
				i.putExtra(BaseActivity.PARAM_FROM_NEXT_STEP_ASSISTANT, true);
				startActivity(i);
				finish();

			}
		});

		this.btnInsert.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String chapter = NewSubjectForm3Activity.this.editTChapter
						.getText().toString();
				
				getApp().getNewSubject().getChapters().add(chapter);
				
				NewSubjectForm3Activity.this.adapter.notifyDataSetChanged();
				
			}
		});

	}

}
