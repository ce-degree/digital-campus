package com.edu.salle.digitalcampus.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import com.edu.salle.digitalcampus.R;

public class MenuActivity extends BaseActivity {

	private ImageButton btnManageStudents;
	private ImageButton btnManageSubjects;
	private ImageButton btnTests;
	private ImageButton btnExit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_menu);
		
		associateControls();
		prepareView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
			switch (item.getItemId()) {
			case R.id.actionbar_add_subject:
                Intent i = new Intent(MenuActivity.this, NewSubjectForm1Activity.class);
                startActivity(i);
                finish();
				break;
				
			case R.id.actionbar_add_student:
				break;
				
			case R.id.actionbar_add_test:
				break;
				
			case R.id.actinobar_exit:
				break;

			default:
				break;
			}
		return true;
	}

	protected void associateControls() {
		this.btnManageStudents = (ImageButton) findViewById(R.id.menu_ManageStudents);
		this.btnManageSubjects = (ImageButton) findViewById(R.id.menu_ManageSubjects);
		this.btnTests = (ImageButton) findViewById(R.id.menu_Tests);
		this.btnExit = (ImageButton) findViewById(R.id.menu_Exit);

	}

	protected void prepareView() {
		this.btnManageStudents.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "Manage Students",
						Toast.LENGTH_LONG).show();
			}
		});
		this.btnManageSubjects.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Toast.makeText(getApplicationContext(), "Manage Subjects",
				// Toast.LENGTH_LONG).show();
				Intent i = new Intent(getApplicationContext(),
						SubjectsListActivity.class);
				startActivity(i);
			}
		});
		this.btnTests.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "Tests",
						Toast.LENGTH_LONG).show();
			}
		});
		this.btnExit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(getApplicationContext(), "Exit",
						Toast.LENGTH_LONG).show();
			}
		});

	}

}
