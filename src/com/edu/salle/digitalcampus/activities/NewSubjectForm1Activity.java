package com.edu.salle.digitalcampus.activities;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.objects.Student;
import com.edu.salle.digitalcampus.objects.Subject;
import com.edu.salle.digitalcampus.util.Tuple2;

public class NewSubjectForm1Activity extends BaseActivity {

	Button btnNext;
	EditText editTSubjectTitle;
	EditText editTSubjectDescription;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_subject_form1);
		this.associateControls();
		this.prepareActivity();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_subject_form1, menu);
		return true;
	}

	private void associateControls() {
		this.btnNext = (Button) this
				.findViewById(R.id.new_subject_form1_btn_next);
		this.editTSubjectTitle = (EditText) this
				.findViewById(R.id.new_subject_form1_title);
		this.editTSubjectDescription = (EditText) this
				.findViewById(R.id.new_subject_form1_description);
	}

	private void prepareActivity() {
		if (getIntent().getExtras() != null) {
			if (getIntent().getExtras().getBoolean(
					BaseActivity.PARAM_FROM_NEXT_STEP_ASSISTANT, false) == true) {
				Toast.makeText(
						NewSubjectForm1Activity.this.getApplicationContext(),
						"Entrando en getExtras", Toast.LENGTH_LONG).show();
				this.editTSubjectTitle.setText(getApp().getNewSubject()
						.getName());
				this.editTSubjectDescription.setText(getApp().getNewSubject()
						.getDescription());
			}
		} else {
			super.getApp()
					.initializeSubjectForCreation(
							new Subject(),
							new Tuple2<ArrayList<Student>, ArrayList<Boolean>>(
									new ArrayList<Student>(),
									new ArrayList<Boolean>()));
		}

		this.btnNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String subjectTitle = NewSubjectForm1Activity.this.editTSubjectTitle
						.getText().toString();
				String subjectDescription = NewSubjectForm1Activity.this.editTSubjectDescription
						.getText().toString();

				if (subjectTitle.length() == 0) {
					Toast.makeText(
							NewSubjectForm1Activity.this
									.getApplicationContext(),
							"Title cannot be empty", Toast.LENGTH_LONG).show();
				}

				if (subjectDescription.length() == 0) {
					Toast.makeText(
							NewSubjectForm1Activity.this
									.getApplicationContext(),
							"Description cannot be empty", Toast.LENGTH_LONG)
							.show();
				}

				if (subjectTitle.length() != 0
						&& subjectDescription.length() != 0) {

					getApp().getNewSubject().setName(subjectTitle);
					getApp().getNewSubject().setDescription(subjectDescription);

					Intent i = new Intent(getApplicationContext(),
							NewSubjectForm2Activity.class);
					startActivity(i);
					finish();
				}

			}
		});
	}

}
