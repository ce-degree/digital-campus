package com.edu.salle.digitalcampus.activities;

import java.io.IOException;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.dao.BaseDAO;

public class MainActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		BaseDAO baseDAO = new BaseDAO(getApplicationContext());
		if (!baseDAO.checkDataBase()) {
			try {
				baseDAO.copyDataBase();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
        new Handler().postDelayed(new Runnable() {
        	 
            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */
 
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(i);
 
                finish();
            }
        }, 2000);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

}
