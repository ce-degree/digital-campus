package com.edu.salle.digitalcampus.objects;

public class Student {

	private String name;
	private String birthDate;
	private String career;
	private String gender;
	private int image;
	
	public Student(String name, String birthDate, String career, String gender,
			int image) {
		this.name = name;
		this.birthDate = birthDate;
		this.career = career;
		this.gender = gender;
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public String getCareer() {
		return career;
	}

	public String getGender() {
		return gender;
	}

	public int getImage() {
		return image;
	}	
	
}
