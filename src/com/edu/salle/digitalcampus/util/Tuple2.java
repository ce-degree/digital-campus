package com.edu.salle.digitalcampus.util;

public class Tuple2<type1, type2> {
	type1 _1;
	type2 _2;
	
	public Tuple2(type1 obj1, type2 obj2) {
		_1 = obj1;
		_2 = obj2;
	}

	public type1 get_1() {
		return _1;
	}

	public void set_1(type1 _1) {
		this._1 = _1;
	}

	public type2 get_2() {
		return _2;
	}

	public void set_2(type2 _2) {
		this._2 = _2;
	}
	
}
