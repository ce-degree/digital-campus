package com.edu.salle.digitalcampus.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.objects.Subject;

public class SubjectAdapter extends ArrayAdapter<Subject> {
	private List<Subject> data;
	private int my_layout;

	public SubjectAdapter(Context context, int resource, List<Subject> data) {
		super(context, resource);
		this.data = data;
		this.my_layout = resource;
	}
	
	@Override
	public int getCount() {
		return data.size();
	}
	
	@Override
	public Subject getItem(int position) {
		return data.get(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		//1. Create view
		if (row == null){
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(my_layout, parent, false);
			row.setTag(position);
			row.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					int pos = Integer.parseInt(v.getTag().toString());
					String text = "Position click "+pos;
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
				}
			});
		}
		
		//2. Capture controls of the view
		ImageView icon = (ImageView) row.findViewById(R.id.subject_list_icon);
		TextView name = (TextView) row.findViewById(R.id.subject_list_name);
		TextView description = (TextView) row.findViewById(R.id.subject_list_description);
		ImageButton delete = (ImageButton) row.findViewById(R.id.subject_list_delete);
		
		//3. Assign new values to captured control
		icon.setImageResource(data.get(position).getImage());
		name.setText(data.get(position).getName());
		name.setTextColor(Color.BLACK);
		description.setText(data.get(position).getDescription()); //TODO TRUNCATE DESCRIPTION
		description.setTextColor(Color.BLACK);
		
		delete.setTag(position);
		delete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {				
				int pos = Integer.parseInt(arg0.getTag().toString());
				String text = "Position delete "+pos;
				Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
			}
		});
		
		//4. Return view;
		return row;
	}

}
