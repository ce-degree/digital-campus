package com.edu.salle.digitalcampus.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.objects.Subject;

public class ChapterAdapter extends ArrayAdapter<String> {
	private List<String> data;
	private int my_layout;

	public ChapterAdapter(Context context, int resource, List<String> data) {
		super(context, resource);
		this.data = data;
		this.my_layout = resource;
	}
	
	@Override
	public int getCount() {
		return data.size();
	}
	
	@Override
	public String getItem(int position) {
		return data.get(position);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		
		//1. Create view
		if (row == null){
			LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(my_layout, parent, false);
			row.setTag(position);
			row.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					int pos = Integer.parseInt(v.getTag().toString());
					String text = "Position click "+pos;
					Toast.makeText(getContext(), text, Toast.LENGTH_SHORT).show();
				}
			});
		}
		
		//2. Capture controls of the view
		ImageButton btnUp = (ImageButton) row.findViewById(R.id.chapter_list_move_up);
		ImageButton btnDown = (ImageButton) row.findViewById(R.id.chapter_list_move_down);
		ImageButton btnDelete = (ImageButton) row.findViewById(R.id.chapter_list_delete);
		TextView name = (TextView) row.findViewById(R.id.chapter_list_name);
		name.setText(position + ". " + data.get(position));
		
		//3. Assign new values to captured control
		name.setTag(position);
		btnUp.setTag(position);
		btnDown.setTag(position);
		btnDelete.setTag(position);
		
		btnUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int pos = Integer.parseInt(v.getTag().toString());
				String tmpChapter = data.get(pos);
				if(pos != 0) {
					data.set(pos , data.get(pos - 1));
					data.set(pos - 1, tmpChapter);
					v.setTag(pos - 1);
					notifyDataSetChanged();
				}
			}
		});
		
		btnDown.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int pos = Integer.parseInt(v.getTag().toString());
				String tmpChapter = data.get(pos);
				if(pos != data.size() - 1) {
					data.set(pos, data.get(pos + 1));
					data.set(pos + 1, tmpChapter);
					v.setTag(pos + 1);
					notifyDataSetChanged();
				}
				
			}
		});

		btnDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int pos = Integer.parseInt(v.getTag().toString());
				data.remove(pos);
				notifyDataSetChanged();
			}
		});
		
		//4. Return view;
		return row;
	}
	
	public void setData(ArrayList<String> data) {
		this.data = data;
	}

}
