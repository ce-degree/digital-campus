package com.edu.salle.digitalcampus.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.objects.Subject;

public class SubjectDAO extends BaseDAO {

	private static final String SUBJECT_TABLE_NAME = "Subject";
	private static final String SUBJECT_CHAPTER_TABLE_NAME = "SubjectChapter";
	
	public SubjectDAO(Context context) {
		super(context);
	}

	public List<Subject> getAllSubjects() {
		List<Subject> subjectsList = new ArrayList<Subject>();
		super.openDBRead();
		
		String sql = "SELECT * FROM "+SUBJECT_TABLE_NAME;
		Cursor cursor = super.myDataBase.rawQuery(sql, null);
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			Subject a = LoadSubjectFromCursor(cursor);
			a.setChapters(getSubjectChapters(a.getName()));
			subjectsList.add(a);
			cursor.moveToNext();
		}
		cursor.close();
		super.closeDB();
		return subjectsList;
	}
	
	
	private List<String> getSubjectChapters(String subjectName) {
		List<String> chapterList = new ArrayList<String>();
		
		String sql = "SELECT name FROM "+SUBJECT_CHAPTER_TABLE_NAME+" WHERE subjectName = ?";
		Cursor cursor = super.myDataBase.rawQuery(sql, new String []{subjectName});
		
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			String a = LoadChapterFromCursor(cursor);
			chapterList.add(a);
			cursor.moveToNext();
		}
		cursor.close();
		
		return chapterList;
	}

	private Subject LoadSubjectFromCursor(Cursor cursor){
		String name = cursor.getString(cursor.getColumnIndex("name"));
		String description = cursor.getString(cursor.getColumnIndex("description"));
		int image = R.drawable.ic_launcher;
		return new Subject(name,description,image,null);
	}
	
	private String LoadChapterFromCursor(Cursor cursor){
		String name = cursor.getString(cursor.getColumnIndex("name"));
		return name;
	}
}
