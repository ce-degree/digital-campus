package com.edu.salle.digitalcampus.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;

import com.edu.salle.digitalcampus.R;
import com.edu.salle.digitalcampus.objects.Student;

public class StudentDAO extends BaseDAO {

	private static final String STUDENT_TABLE_NAME = "Student";
	private static final String SUBJECT_STUDENT_TABLE_NAME = "SubjectStudent";
	
	public StudentDAO(Context context) {
		super(context);
	}

	public List<Student> getAllStudents() {
		List<Student> studentsList = new ArrayList<Student>();
		super.openDBRead();
		
		String sql = "SELECT * FROM "+STUDENT_TABLE_NAME;
		Cursor cursor = super.myDataBase.rawQuery(sql, null);
		cursor.moveToFirst();
		while (cursor.isAfterLast() == false) {
			Student a = LoadStudentFromCursor(cursor);
			studentsList.add(a);
			cursor.moveToNext();
		}
		cursor.close();
		super.closeDB();
		return studentsList;
	}

	private Student LoadStudentFromCursor(Cursor cursor){
		String name = cursor.getString(cursor.getColumnIndex("name"));
		String birthDate = cursor.getString(cursor.getColumnIndex("birthDate"));
		String career = cursor.getString(cursor.getColumnIndex("career"));
		int type = cursor.getInt(cursor.getColumnIndex("gender"));
		
		String gender = null;
		if (type == 1) gender = "male";
		else gender = "female";
		
		int image = R.drawable.ic_launcher;
		
		return new Student(name, birthDate, career, gender, image);
	}
}
